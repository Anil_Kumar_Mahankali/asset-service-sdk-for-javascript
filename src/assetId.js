export default class AssetId{

    /*
     fields
     */
    _id:string;

    constructor(id:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;
    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    toJSON() {
        return {
            assetId: this._id
        }
    }
}
