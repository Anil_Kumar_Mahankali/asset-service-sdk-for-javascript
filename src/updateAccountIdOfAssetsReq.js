export default class UpdateAccountIdOfAssetsReq {

    _accountId:string;

    _assetIds:Array<string>;

    /**
     * @param {string} accountId
     * @param {string[]} assetIds
     */
    constructor(accountId:string,
                assetIds:Array<string>) {

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if (!assetIds) {
            throw new TypeError('assetIds required');
        }
        this._assetIds = assetIds;

    }

    get accountId():string {
        return this._accountId;
    }

    get assetIds():Array<string> {
        return this._assetIds;
    }

    toJSON() {
        return {
            accountId: this._accountId,
            assetIds: this._assetIds
        };
    }
}