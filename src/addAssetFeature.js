import {inject} from 'aurelia-dependency-injection';
import AssetServiceSdkConfig from './assetServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client'
import AddAssetReq from './addAssetReq';
import AssetId from './assetId';

@inject(AssetServiceSdkConfig, HttpClient)
class AddAssetFeature {

    _config:AssetServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:AssetServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Adds a asset
     * @param {AddAssetReq} request
     * @param {string} accessToken
     * @returns {Promise.<AssetId>} id
     */
    execute(request:AddAssetReq,
            accessToken:string):Promise<AssetId> {

        return this._httpClient
            .createRequest('assets')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);

    }
}

export default AddAssetFeature;