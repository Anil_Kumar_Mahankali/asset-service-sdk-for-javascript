Feature: List Assets With Account Id
  Lists all assets with the provided account id

  Scenario: Success
    Given I provide an accountId that matches assets in the asset-service
    And provide an accessToken identifying me as a partner rep associated with accountId
    When I execute listAssetsWithAccountId
    Then the assets are returned