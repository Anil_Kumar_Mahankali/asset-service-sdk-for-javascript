Feature: List Assets With Serial Number
  Lists all assets with the provided serial number

  Scenario: Success
    Given I provide a serial number that matches assets in the asset-service
    And provide an accessToken identifying me as a partner rep associated with accountId
    When I execute listAssetsWithSerialNumber
    Then the assets are returned